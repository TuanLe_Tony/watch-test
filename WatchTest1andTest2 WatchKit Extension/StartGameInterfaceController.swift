//
//  StartGameInterfaceController.swift
//  WatchTest1andTest2 WatchKit Extension
//
//  Created by levantuan on 2019-03-14.
//  Copyright © 2019 levantuan. All rights reserved.
//

import WatchKit
import Foundation


class StartGameInterfaceController: WKInterfaceController {
    
    @IBOutlet weak var NameLabel: WKInterfaceLabel!
    let easy = "easy"
    let hard = "hard"
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        let name = UserDefaults.standard.string(forKey: "name")
        self.NameLabel.setText("Hello \(name!)")
        
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

    @IBAction func EasyButtonClicked() {
        print("Easy Button clicked")
        UserDefaults.standard.set(easy, forKey: "game")
        
        presentController(withName: "Game", context: nil)
        
    }
    @IBAction func HardButtonClicked() {
        print("Easy Button clicked")
        UserDefaults.standard.set(hard, forKey: "game")
        presentController(withName: "Game", context: nil)
       
    }
    
}
