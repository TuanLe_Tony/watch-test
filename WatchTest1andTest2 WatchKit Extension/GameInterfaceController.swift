//
//  GameInterfaceController.swift
//  WatchTest1andTest2 WatchKit Extension
//
//  Created by levantuan on 2019-03-14.
//  Copyright © 2019 levantuan. All rights reserved.
//

import WatchKit
import Foundation


class GameInterfaceController: WKInterfaceController {
    
    @IBOutlet weak var sequenceLabel: WKInterfaceLabel!
    @IBOutlet weak var showImage1: WKInterfaceImage!
    
    @IBOutlet weak var showImage2: WKInterfaceImage!
    @IBOutlet weak var showImage3: WKInterfaceImage!
    @IBOutlet weak var showImage4: WKInterfaceImage!
    
    @IBOutlet weak var strawberry: WKInterfaceButton!
    @IBOutlet weak var orangeButton: WKInterfaceButton!
    @IBOutlet weak var cherryButton: WKInterfaceButton!
    @IBOutlet weak var bananaButton: WKInterfaceButton!
    //for hard game
    var GetRandomArray = ["banana","cherry", "strawberry", "orange"]
    var myArray = [String]()
    var empty = [String]()
    //
    // for easy game
    var GetRandomArray1 = ["banana","cherry", "orange"]
    var myArray1 = [String]()
    var empty1 = [String]()
    //
    
    // count down
    var seconds = 20
    var timer = Timer()
    //
    let game = UserDefaults.standard.string(forKey: "game")
   
    
    
    
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        print("\(game!)")
        if ((game!) == "hard"){
            runHardGame()
        }
        if ((game!) == "easy"){
            runEasyGame1()
        }
    

//        self.showImage1.setImageNamed("banana")
//        self.showImage2.setImageNamed("cherry")
//        self.showImage3.setImageNamed("strawberry")
//        self.showImage4.setImageNamed("orange")
        
    
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
 
    @IBAction func bananaButtonClicked() {
        print("Banana clicked")
        empty.append("banana")
        empty1.append("banana")
        print(empty)
        print(empty1)
        //for hard game
        if ((game!) == "hard"){
            if (empty == myArray){
                print("You Win")
            }
            // this is my idea how to put the image back when selected
            if(empty.count == 1){
                 self.showImage1.setHidden(false) // the UI Nonsense is currently hidden from updatetimer --> I just set it back here
                 self.showImage1.setImageNamed("banana")
            }
            if(empty.count == 2){
                 self.showImage2.setHidden(false) // the UI Nonsense is currently hidden from updatetimer --> I just set it back here
                self.showImage2.setImageNamed("banana")
            }
            if(empty.count == 3){
                 self.showImage3.setHidden(false) // the UI Nonsense is currently hidden from updatetimer --> I just set it back here
                self.showImage3.setImageNamed("banana")
            }//end
            if (empty.count == 4){  // it's going to reset the game anyway, so I did not coppy and paste UI nonsense in here :)
                print("array = 4")
                 presentController(withName: "Game", context: nil)// reset game
            }
            
                
            else {
                print("You Lose")
            }
        }
        // for easy game
        if ((game!) == "easy"){
            if (empty1 == myArray1){
                print("You Win")
            }
            if(empty.count == 1){
                self.showImage1.setHidden(false) // the UI Nonsense is currently hidden from updatetimer --> I just set it back here
                self.showImage1.setImageNamed("banana")
            }
            if(empty.count == 2){
                self.showImage2.setHidden(false) // the UI Nonsense is currently hidden from updatetimer --> I just set it back here
                self.showImage2.setImageNamed("banana")
            }
            if (empty.count == 3){
                print("array = 3")
                presentController(withName: "Game", context: nil)// reset game
            }
                
            else {
                print("You Lose")
            }
        }
    }
    
    @IBAction func cherryButtonClicked() {
         print("cherry clicked")
        empty.append("cherry")
        empty1.append("cherry")
        print(empty)
        print(empty1)
        //for hard game
        if ((game!) == "hard"){
            if (empty == myArray){
                print("You Win")
            }
            // this is my idea how to put the image back when selected
            if(empty.count == 1){
                self.showImage1.setHidden(false) // the UI Nonsense is currently hidden from updatetimer --> I just set it back here
                self.showImage1.setImageNamed("cherry")
            }
            if(empty.count == 2){
                 self.showImage2.setHidden(false) // the UI Nonsense is currently hidden from updatetimer --> I just set it back here
                self.showImage2.setImageNamed("cherry")
            }
            if(empty.count == 3){
                 self.showImage3.setHidden(false) // the UI Nonsense is currently hidden from updatetimer --> I just set it back here
                self.showImage3.setImageNamed("cherry")
            }//end
            if (empty.count == 4){
                print("array = 4")
                presentController(withName: "Game", context: nil)// reset game
            }
                
            else {
                print("You Lose")
            }
        }
        // for easy game
        if ((game!) == "easy"){
            if (empty1 == myArray1){
                print("You Win")
            }
            if(empty.count == 1){
                self.showImage1.setHidden(false) // the UI Nonsense is currently hidden from updatetimer --> I just set it back here
                self.showImage1.setImageNamed("cherry")
            }
            if(empty.count == 2){
                self.showImage2.setHidden(false) // the UI Nonsense is currently hidden from updatetimer --> I just set it back here
                self.showImage2.setImageNamed("cherry")
            }
            if (empty.count == 3){ // it's going to reset the game anyway, so I did not coppy and paste UI Nonsense here
                print("array = 3")
                presentController(withName: "Game", context: nil)// reset game
            }
                
            else {
                print("You Lose")
            }
        }
    }
    @IBAction func OrangerButtonClicked() {
         print("Orange clicked")
        empty.append("orange")
        empty1.append("orange")
        print(empty)
        print(empty1)
        //for hard game
        if ((game!) == "hard"){
            if (empty == myArray){
                print("You Win")
            }
            // this is my idea how to put the image back when selected
            if(empty.count == 1){
                 self.showImage1.setHidden(false) // the UI Nonsense is currently hidden from updatetimer --> I just set it back here
                self.showImage1.setImageNamed("orange")
            }
            if(empty.count == 2){
                 self.showImage2.setHidden(false) // the UI Nonsense is currently hidden from updatetimer --> I just set it back here
                self.showImage2.setImageNamed("orange")
            }
            if(empty.count == 3){
                 self.showImage3.setHidden(false) // the UI Nonsense is currently hidden from updatetimer --> I just set it back here
                self.showImage3.setImageNamed("orange")
            }//end
            if (empty.count == 4){
                print("array = 4")
                presentController(withName: "Game", context: nil)// reset game
            }
                
            else {
                print("You Lose")
            }
        }
        // for easy game
        if ((game!) == "easy"){
            if (empty1 == myArray1){
                print("You Win")
            }
            if(empty.count == 1){
                self.showImage1.setHidden(false) // the UI Nonsense is currently hidden from updatetimer --> I just set it back here
                self.showImage1.setImageNamed("orange")
            }
            if(empty.count == 2){
                self.showImage2.setHidden(false) // the UI Nonsense is currently hidden from updatetimer --> I just set it back here
                self.showImage2.setImageNamed("orange")
            }
            if (empty.count == 3){
                print("array = 3")
                presentController(withName: "Game", context: nil)// reset game
            }
                
            else {
                print("You Lose")
            }
        }
    }
    // strawberry button does not use for easy game
    @IBAction func strawbeeryButtonClicked() {
        print("strawberry clicked")
        empty.append("strawberry")
        print(empty)
        if (empty == myArray){
            print("You Win")
        }
        // this is my idea how to put the image back when selected
        if(empty.count == 1){
             self.showImage1.setHidden(false) // the UI Nonsense is currently hidden from updatetimer --> I just set it back here
            self.showImage1.setImageNamed("strawberry")
        }
        if(empty.count == 2){
             self.showImage2.setHidden(false) // the UI Nonsense is currently hidden from updatetimer --> I just set it back here
            self.showImage2.setImageNamed("strawberry")
        }
        if(empty.count == 3){
             self.showImage3.setHidden(false) // the UI Nonsense is currently hidden from updatetimer --> I just set it back here
            self.showImage3.setImageNamed("strawberry")
        }//end
        if (empty.count == 4){
            print("array = 4")
            presentController(withName: "Game", context: nil)//  reset game
        }
        else {
            print("You Lose")
        }
    }
    
    func runHardGame() {
        timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(GameInterfaceController.updateTimer)), userInfo: nil, repeats: true)
    }
    func runEasyGame1() {
        timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(GameInterfaceController.updateTimer1)), userInfo: nil, repeats: true)
    }
    
    
    @objc func updateTimer() {
        seconds -= 1     //This will decrement(count down)the seconds.
        if (seconds == 19){
            if let randomElement = GetRandomArray.randomElement() { // this Code from internet =.=
                print(randomElement)
                myArray.append(randomElement)
                print(myArray)
                self.showImage1.setImageNamed(randomElement)
            }
//            self.showImage1.setImageNamed("banana")
        }
        if (seconds == 17){
            if let randomElement1 = GetRandomArray.randomElement() { // this Code from internet =.=
                print(randomElement1)
                myArray.append(randomElement1)
                print(myArray)
                self.showImage2.setImageNamed(randomElement1)
            }
//            self.showImage2.setImageNamed("cherry")
        }
        if (seconds == 15){
            if let randomElement2 = GetRandomArray.randomElement() { // this Code from internet =.=
                print(randomElement2)
                myArray.append(randomElement2)
                print(myArray)
                self.showImage3.setImageNamed(randomElement2)
            }
//        self.showImage3.setImageNamed("strawberry")
        }
        if (seconds == 13){
            if let randomElement3 = GetRandomArray.randomElement() { // this Code from internet =.=
                print(randomElement3)
                myArray.append(randomElement3)
                print(myArray)
                self.showImage4.setImageNamed(randomElement3)
            }
//        self.showImage4.setImageNamed("orange")
        }
        if (seconds == 9)
        {
            self.showImage1.setHidden(true)
            self.showImage2.setHidden(true)
            self.showImage3.setHidden(true)
            self.showImage4.setHidden(true)
            
            self.bananaButton.setBackgroundImageNamed("banana")
            self.cherryButton.setBackgroundImageNamed("cherry")
            self.orangeButton.setBackgroundImageNamed("orange")
            self.strawberry.setBackgroundImageNamed("strawberry")
            
            self.sequenceLabel.setText("Repeat by Order")
        }
    }
    @objc func updateTimer1() {
        seconds -= 1     //This will decrement(count down)the seconds.
        if (seconds == 19){
            if let randomElement4 = GetRandomArray1.randomElement() { // this Code from internet =.=
                print(randomElement4)
                myArray1.append(randomElement4)
                print(myArray1)
                self.showImage1.setImageNamed(randomElement4)
            }
//            self.showImage1.setImageNamed("orange")
        }
        if (seconds == 17){
            if let randomElement5 = GetRandomArray1.randomElement() { // this Code from internet =.=
                print(randomElement5)
                myArray1.append(randomElement5)
                print(myArray1)
                self.showImage2.setImageNamed(randomElement5)
            }
//            self.showImage2.setImageNamed("cherry")
        }
        if (seconds == 15){
            if let randomElement6 = GetRandomArray1.randomElement() { // this Code from internet =.=
                print(randomElement6)
                myArray1.append(randomElement6)
                print(myArray1)
                self.showImage3.setImageNamed(randomElement6)
            }
//            self.showImage3.setImageNamed("banana")
        }
        
        if (seconds == 10)
        {
            self.showImage1.setHidden(true)
            self.showImage2.setHidden(true)
            self.showImage3.setHidden(true)
            
            self.bananaButton.setBackgroundImageNamed("banana")
            self.cherryButton.setBackgroundImageNamed("cherry")
            self.orangeButton.setBackgroundImageNamed("orange")
            self.strawberry.setHidden(true)
        
            
            self.sequenceLabel.setText("Repeat Start")
        }
    }
}
