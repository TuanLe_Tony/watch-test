//
//  InterfaceController.swift
//  WatchTest1andTest2 WatchKit Extension
//
//  Created by levantuan on 2019-03-14.
//  Copyright © 2019 levantuan. All rights reserved.
//

import WatchKit
import Foundation


class InterfaceController: WKInterfaceController {

    @IBOutlet weak var Namelabel: WKInterfaceLabel!
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        
        let suggestedRespones = ["NoOne"]
        presentTextInputController(withSuggestions: suggestedRespones, allowedInputMode:.plain) { (results) in
            // Write code to process person's respon
            if (results != nil && results!.count > 0) {
                let userResponse = results?.first as? String
                self.Namelabel.setText(userResponse)
                
                UserDefaults.standard.set(userResponse, forKey: "name")
            }
            
        }
        let name = UserDefaults.standard.string(forKey: "name")
        presentController(withName: "StartGame", context: nil)
        
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
   
    

}
